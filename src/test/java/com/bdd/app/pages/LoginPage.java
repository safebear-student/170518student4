package com.bdd.app.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by CCA_Student on 06/12/2017.
 */
public class LoginPage extends BasePage{

    WebDriver driver;
    @FindBy(id = "user")
    WebElement username_field;
    @FindBy(id = "pass")
    WebElement password_field;

    @FindBy(name = "submit")
    WebElement login_button;

    public LoginPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver,this);

    }

    public void enterUsername(String username){
        username_field.sendKeys(username);
    }

    public void enterPassword(String password){
        password_field.sendKeys(password);
    }

    public void login(){
        login_button.click();
    }




}
