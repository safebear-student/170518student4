package com.bdd.app;

import cucumber.api.CucumberOptions;
import cucumber.api.Plugin;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;


/**
 * first test done in cucumber 17/05/2018.
 */

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"pretty", "html:target/cucumber"},
        tags ="~@to-do",
        glue ="com.bdd.app",
        features ="classpath:simplerisk.features/user_management.feature"
)
public class RunCukesTest {
}
