package com.bdd.app;

import com.bdd.app.pages.ConfigurePage;
import com.bdd.app.pages.LoginPage;
import com.bdd.app.pages.ReportingPage;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

/**
 * Created by cca_student on 17/05/2018.
 */
public class StepDefs {

    WebDriver driver;
    String url;
    LoginPage loginpage;
    ConfigurePage configurePage;
    ReportingPage reportingPage;

    @Before
    public void setup() {
        driver = new ChromeDriver();
        url = "http://simplerisk.local/index.php";
        loginpage = new LoginPage(driver);
        configurePage = new ConfigurePage(driver);
        reportingPage = new ReportingPage(driver);

        driver.get(url);
        driver.manage().window().maximize();
        ;
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @After

    public void tearDown() {
        try {
            reportingPage.logout();
        } catch (Exception e) {
        }
        driver.quit();
    }

    @Given("^a user called (.+) with (.+) permissons and password (.+)$")
    public void a_user_is_created(String username, String permissions, String password) throws Throwable {

        loginpage.enterUsername("admin");
        loginpage.enterPassword("^Y&U8i9o0p");
        loginpage.login();

        reportingPage.clickConfigureLink();

        configurePage.clickUserManagementLink();

        if (configurePage.checkIfUserIsPresent(username)) {
            configurePage.logout();
        } else {
            configurePage.enterNewUsername(username);
            configurePage.enterNewPassword(password);
            configurePage.givePermissions(permissions);
            configurePage.createUser();
            configurePage.logout();
        }
    }


    @When("^(.+) i logged in with the password (.+)$")
    public void a_user_is_logged_in(String username, String password) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        loginpage.enterUsername(username);
        loginpage.enterPassword(password);
        loginpage.login();
    }

    @Then("^he is able to view (.+)'s account$")
    public void he_is_able_to_view_user_account(String user)throws Throwable {
        reportingPage.clickConfigureLink();
        configurePage.clickUserManagementLink();
        configurePage.viewUserDetails(user);
        assertTrue (configurePage.checkUserDetails(user));


    }
}
