Feature: User Management

  User Story:
  In order to manage users
  As an administrator to user accounts

  Rules:
  - Non admin users have no permissions ver other accounts
  - There are two types of the non-admin accounts:
  --Risk managers
  --Asset managers
  --Non admin users must be only to be able to see risks or assets in their group
  -- Admin must be only able to reset passwords
  --when an account is created a user must be forced to reset their password on
  first login


  Questions:
  - Do admin users need to access to all or only the accounts in their group

  To Do:
  - Force reset of password on account creation

  Domain language:
  Group = users defined by which group they belong to
  CRUD = Create, Read, Update, Delete Administrator permissions = access to CRUD risks, users and assets for all groups
  risk management permissions = Only able to CRUD risks
  asset_management permissions= Only able to CRUD assets

  Background:
    Given a user called simon with administrator permissons and password S@feb3ar
    And a user called tom with risk_management permissons and password S@feb3ar

    @high-impact
    Scenario Outline: the admininistrator checks a user's detals
      When simon i logged in with the password S@feb3ar
      Then he is able to view <user>'s account
      Examples:
        | user |
        |simon  |

      @high-risk
      @to-do
        Scenario Outline: A user's password is reset by the administrator
        Given a <user> has lost his password
        When simon is logged in with password S@feb3ar
        Then simon can reset <user's password
        Examples:
          | user |
         |tom   |


